def staircase(_n):
    result = ''
    for i in range(1, _n+1):
        result += f'{"#"*i: >{_n}}\n'

    return result.rstrip()
