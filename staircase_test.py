import staircase
import unittest

class TestStaircase(unittest.TestCase):
    def test_give_1_should_be_h(self):
        result = staircase.staircase(1)
        self.assertEqual(result, '#', 'Should be "#"')

    def test_give_2_should_be_hh(self):
        result = staircase.staircase(2)
        self.assertEqual(result, ' #\n##', 'Should be " #\n##"')

    def test_give_3_should_be_hhh(self):
        result = staircase.staircase(3)
        expected = \
        '    #\n'+\
        '   ##\n'+\
        '  ###\n'
            
        self.assertEqual(result, expected, f'Should be {expected}')
    
    def test_give_4_should_be_hhhh(self):
        result = staircase.staircase(4)
        expected = \
        '    #\n'+\
        '   ##\n'+\
        '  ###\n'+\
        ' ####\n'
            
        self.assertEqual(result, expected, f'Should be {expected}')

    def test_give_5_should_be_hhhhh(self):
        result = staircase.staircase(5)
        expected = \
        '    #\n'+\
        '   ##\n'+\
        '  ###\n'+\
        ' ####\n'+\
        '#####'
            
        self.assertEqual(result, expected, f'Should be {expected}')
